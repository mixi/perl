# Copyright 2009 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require perl-module [ module_author=IOANR buildsystem=module-build ]

SUMMARY="A new kind of database designed for the post Web-2.0 world"
DESCRIPTION="
Prophet is a new kind of database designed for the post Web-2.0 world. It's
made to let you collaborate with your friends and coworkers without needing any
kind of special server or Internet provider.

Prophet's buzzword-laden pitch reads something like this:
\"A grounded, semirelational, peer to peer replicated, disconnected, versioned,
property database with self-healing conflict resolution.\"
"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-perl/Module-Build[>=0.3601]
    build+run:
        dev-perl/Any-Moose[>=0.04]
        dev-perl/Config-GitLike[>=1.02]
        dev-perl/Exporter-Lite
        dev-perl/HTTP-Date
        dev-perl/IPC-Run3
        dev-perl/JSON[>=2.00]
        dev-perl/libwww-perl [[ note = [ LWP::UserAgent ] ]]
        dev-perl/MIME-Base64-URLSafe
        dev-perl/Mouse[>=0.89]
        dev-perl/Params-Validate
        dev-perl/Path-Dispatcher[>=1.02]
        dev-perl/Path-Dispatcher-Declarative[>=0.03]
        dev-perl/Proc-InvokeEditor
        dev-perl/Time-Progress
        dev-perl/URI
        dev-perl/UUID-Tiny[>=1.02]
        dev-perl/XML-Atom-SimpleFeed
    run:
        dev-perl/TermReadKey
    test:
        dev-perl/HTTP-Server-Simple[>=0.40]
        dev-perl/Test-Exception[>=0.26]
        dev-perl/Template-Declare[>=0.35] [[ note = [ Optional, but fails tests without it ] ]]
    suggestion:
        dev-perl/JSON-XS[>=2.23] [[ description = [ Faster JSON Parsing ] ]]
        (
            dev-perl/DBI
            dev-perl/DBD-SQLite
        ) [[ description = [ SQLite replica support ] ]]
        dev-perl/Template-Declare [[ description = [ HTML display ] ]]
        (
            dev-perl/File-ShareDir[>=1.00]
            dev-perl/HTTP-Server-Simple[>=0.40]
        ) [[
            *description = [ Web server support ]
            *group-name = [ web-server ]
        ]]
"

# Some tests are broken because they rely on line-based output to be emitted
# in a specific order - https://rt.cpan.org/Public/Bug/Display.html?id=93807
RESTRICT="test"

src_test() {
    # https://rt.cpan.org/Public/Bug/Display.html?id=88537#txn-1337555
    export ANY_MOOSE=Moose

    perl-module_src_test
}

