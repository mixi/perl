# Copyright 2009 Anders Ossowicki <arkanoid@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PNV="${PN}-v${PV}"

require perl-module [ module_author=SKAJI buildsystem=module-build ]

SUMMARY="A lightweight version of the Moose framework"
DESCRIPTION="Moose is wonderful. Unfortunately, it's a little slow. Though
significant progress has been made over the years, the compile time penalty
is a non-starter for some applications. Mouse aims to alleviate this by
providing a subset of Moose's functionality, faster. In particular, 'has' in
Moose is missing only a few expert-level features. We're also going as light
on dependencies as possible. Class::Method::Modifiers or Data::Util is
required if you want support for 'before', 'after', and 'around'"

SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        dev-perl/Module-Build[>=0.405]
        dev-perl/Module-Build-XSUtil[>=0.19]
    test:
        dev-perl/Test-Exception
        dev-perl/Test-Fatal
        dev-perl/Test-LeakTrace
        dev-perl/Test-Output
        dev-perl/Test-Requires
        dev-perl/Try-Tiny
"

WORK="${WORKBASE}/${MY_PNV}"

pkg_setup() {
    # perl build script fails with default value "x86_64-pc-linux-gnu-ld"
    export LD="${CC}"
}

